file { '/tmp/hello':
    content => 'ok'
}

vcsrepo { '/home/vagrant/src/datahub':
    ensure   => present,
    provider => git,
    source   => 'https://github.com/linkedin/datahub',
    owner    => 'vagrant',
    group    => 'vagrant',
}

java::adopt { 'jdk8':
    ensure  => present,
    version => '8',
    java    => 'jdk'
}

file { '/usr/bin/java':
    ensure => 'link',
    target => '/usr/lib/jvm/jdk8u202-b08/bin/java'
}

class { 'elasticsearch': }

class { 'zookeeper': }

class { 'kafka::broker':
    config => {
        'broker.id'         => '0',
        'zookeeper.connect' => 'localhost:2181'
    }
}

kafka::topic { 'test':
    ensure             => present,
    zookeeper          => 'localhost:2181',
    replication_factor => 1,
    partitions         => 1,
}

# linked from https://neo4j.com/download-thanks/?edition=community&release=4.0.6&flavour=unix
#
$package_name = 'neo4j-community-4.0.6-unix.tar.gz'
$neo4j_download_url = "https://dist.neo4j.org/${package_name}"
$install_path = '/home/vagrant/dist'

file { $install_path:
    ensure => directory,
    owner  => 'vagrant',
    group  => 'vagrant',
    mode   => '0755',
}

archive { 'neo4j-community':
    path         => "/tmp/${package_name}",
    source       => $neo4j_download_url,
    extract      => true,
    extract_path => $install_path,
    creates      => "${install_path}/neo4j",
    require      => File[$install_path]
}

file { '/home/vagrant/dist/neo4j-community-4.0.6/conf/neo4j.conf':
    ensure => present,
    source => '/vagrant/neo4j.conf'
}

file { '/usr/lib/systemd/system/neo4j.service':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => '/vagrant/neo4j.service'
} ~> service { 'neo4j':
    ensure => 'running'
}

package { 'python3-pip':
    ensure => present
}

exec { '/home/vagrant/src/datahub/gradlew build':
    environment => ['JAVA_HOME=/usr/lib/jvm/jdk8u202-b08/'],
    cwd         => '/home/vagrant/src/datahub/',
    # creates     => '/home/vagrant/src/datahub/datahub-frontend/build/',
    logoutput   => true,
    timeout     => 0,
    require     => Package['python3-pip']
}
