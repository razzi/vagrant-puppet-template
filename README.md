# vagrant-puppet-template

A Vagrantfile based on debian 11 that provisions by running a puppet manifest

# requirements

- vagrant
- virtualbox

# run

vagrant up
