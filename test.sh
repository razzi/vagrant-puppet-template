#! /bin/sh
set -e

vagrant destroy -f
vagrant up
vagrant provision --provision-with puppet
vagrant ssh -c 'cat /tmp/hello'
vagrant ssh -c 'ls /usr/local/src/datahub'
